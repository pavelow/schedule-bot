# Discord schedule-bot.sch_api

This folder contains the modules that fetch schedule data.

Currently three modules are implemented:
- Pretalx: For pulling schedule data from [pretalx](https://pretalx.com/p/about/) with a custom `pre_record` field.
- file:    For pulling schedule data from a file formatted the same as the pretalx api with the `pre_record` field.
- Veyepar: For pulling schedule data from [veyepear](https://github.com/xfxf/veypear), a fork of [veyepar](https://github.com/CarlFK/veyepar) with support for virtual presenters.


## Interface
Each module needs to implement the `scheduleWrangler` class with the following functions:

### Event Object
```
class Event(TypedDict):
	title: str
	code: str               # For matching talk in other APIs
	speakers: List[str]     # List of speaker names, will be matched to discord nicknames
	speakerCodes: List[str] # For matching speakers in other APIs
	pre_record: bool        # Pre-recorded talk?
	start: datetime
	room: str               # Room name, will map to discord channel ID via channel_map.json
	end: datetime
```


### __init__
`def __init__(self, logging, timezone, debug="", token=None, url="schedule.json")`   
`logging`: python logging object, for logging activity of the module.   
`timezone`: Conference timezone (tz to use if no tz is specified in talk times)   
`debug`: Time offset to apply to the first imported - to trigger the bot for testing purposes.
`token`: API token (if required), will be passed in from `--token` option.   
`url`: location of the endpoint or file.


### import_talks
`async def import_talks(self)`
Imports events into internal schedule datastructure.

### eventsAtTime
`def eventsAtTime(self, time: int) -> List[Event]:`   
Returns list of events that happen at a specific `unix timestamp` time.

### printEvents
`def printEvents(self) -> None:`   
Prints events datastructure as JSON for debug purposes
