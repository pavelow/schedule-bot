from typing import Dict, List, Optional, TypedDict
from datetime import datetime, timedelta, timezone
from collections import defaultdict
import httpx
import json

class Room(TypedDict):
	en: str


class Slot(TypedDict):
	start: str
	end: str
	room: Room


class Speaker(TypedDict):
	name: str
	code: str


class ApiEvent(TypedDict):
	title: str
	code: str
	slot: Slot
	speakers: List[Speaker]
	internal_notes: str


class Event(TypedDict):
	title: str
	code: str
	speakers: List[str]
	speakerCodes: List[str]
	pre_record: bool
	start: datetime
	room: str
	end: datetime


class scheduleWrangler:
	def __init__(self, logging, debug="", url=None, token=None) -> None:
		self.events: Dict[int, List[Event]] = {}
		self.url = url
		self.logging = logging
		self.token = {"Authorization": "Token {0}".format(token)} # Pretalx token


	# Grabs all results for api query recursively
	async def get_results(self, url) -> List[ApiEvent]:
		logging.info("Get %s", url)
		async with httpx.AsyncClient() as client:
			r = await client.get(url, headers=token)
		response = r.json()
		results = response["results"]

		if response["next"]:
			results.extend(await self.get_results(response["next"]))
		return results

	async def import_talks(self) -> None:
		scheduleJSON = ""
		results: List[ApiEvent] = []

		# Grab from url
		logging.info("Ignoring file, pulling from API")
		results = await self.get_results(self.url + "?format=json&state=confirmed")

		logging.info("loaded %s talks.", len(results))

		# Should have data here, bail if we don't
		if len(results) == 0:
			sys.exit(-1)

		events: Dict[int, List[Event]] = defaultdict(list)
		for event in results:
			startTime = int(datetime.fromisoformat(event["slot"]["start"]).timestamp())

			# Need (room name)  (time-of-talk) <speaker name> - <talk name>>
			# Build internal data structure
			events[startTime].append(
				{
					"title": event["title"],
					"code": event["code"],
					"room": event["slot"]["room"]["en"],
					"speakers": [speaker["name"] for speaker in event["speakers"]],
					"speakerCodes": [speaker["code"] for speaker in event["speakers"]],
					"pre_record": "*PREREC:ACCEPT" in (event["internal_notes"] or ""),
					"start": datetime.fromisoformat(event["slot"]["start"]),
					"end": datetime.fromisoformat(event["slot"]["end"]),
				}
			)
		self.events = dict(events)  # Erase previous data if it existed

	def eventsAtTime(self, time: int) -> List[Event]:
		events = []
		if time in self.events:
			events = self.events[time]
		return events

	def printEvents(self) -> None:
		for event in self.events:
			print(self.events[event])
