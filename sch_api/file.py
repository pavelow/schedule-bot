from typing import Dict, List, Optional, TypedDict
from datetime import datetime, timedelta, timezone
from collections import defaultdict
import json

class Room(TypedDict):
	en: str


class Slot(TypedDict):
	start: str
	end: str
	room: Room


class Speaker(TypedDict):
	name: str
	code: str


class ApiEvent(TypedDict):
	title: str
	code: str
	slot: Slot
	speakers: List[Speaker]
	internal_notes: str


class Event(TypedDict):
	title: str
	code: str
	speakers: List[str]
	speakerCodes: List[str]
	pre_record: bool
	start: datetime
	room: str
	end: datetime

class scheduleWrangler:
	def __init__(self, logging, timezone, debug="", token=None, url="schedule.json") -> None:
		self.events: Dict[int, List[Event]] = {}
		self.url = url
		self.logging = logging
		self.debug = debug

	async def import_talks(self) -> None:
		scheduleJSON = ""
		results: List[ApiEvent] = []

		# Pull schedule from file
		self.logging.info("Using file: %s", self.url)
		with open(self.url) as s:
			results = json.load(s)["results"]

		self.logging.info("loaded %s talks.", len(results))

		# Should have data here, bail if we don't
		if len(results) == 0:
			sys.exit(-1)

		events: Dict[int, List[Event]] = defaultdict(list)
		for event in results:
			startTime = int(datetime.fromisoformat(event["slot"]["start"]).timestamp())

			# Need (room name)  (time-of-talk) <speaker name> - <talk name>>
			# Build internal data structure
			events[startTime].append(
				{
					"title": event["title"],
					"code": event["code"],
					"room": event["slot"]["room"]["en"],
					"speakers": [speaker["name"] for speaker in event["speakers"]],
					"speakerCodes": [speaker["code"] for speaker in event["speakers"]],
					"pre_record": "*PREREC:ACCEPT" in (event["internal_notes"] or ""),
					"start": datetime.fromisoformat(event["slot"]["start"]),
					"end": datetime.fromisoformat(event["slot"]["end"]),
				}
			)
		self.events = dict(events)  # Erase previous data if it existed

		# For debugging
		if self.debug:
			first_event = list(self.events.keys())[0] #grab first key
			t = int(datetime.now().timestamp())+timedelta(minutes=float(self.debug)).seconds #Add fist event with key=now+offset
			self.events[t] = self.events[first_event]
			#schedule.events[t][0]["end"] = datetime.now()

	def eventsAtTime(self, time: int) -> List[Event]:
		events = []
		if time in self.events:
			events = self.events[time]
		return events

	def printEvents(self) -> None:
		for event in self.events:
			print(self.events[event])
