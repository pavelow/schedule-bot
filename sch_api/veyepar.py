from typing import Dict, List, Optional, TypedDict
from datetime import datetime, timedelta, timezone
import pytz
from collections import defaultdict
import httpx
import json
import asyncio

class Room(TypedDict):
	en: str


class Slot(TypedDict):
	start: str
	end: str
	room: Room


class Speaker(TypedDict):
	name: str
	code: str


class ApiEvent(TypedDict):
	title: str
	code: str
	slot: Slot
	speakers: List[Speaker]
	internal_notes: str


class Event(TypedDict):
	title: str
	code: str
	speakers: List[str]
	speakerCodes: List[str]
	pre_record: bool
	start: datetime
	room: str
	end: datetime


class scheduleWrangler:
	def __init__(self, logging, timezone, debug="", token=None, url=None) -> None:
		self.events: Dict[int, List[Event]] = {}
		self.token = token
		self.url = url
		self.logging = logging
		self.timezone = timezone

	async def import_talks(self) -> None:
		scheduleJSON = ""
		results: List[ApiEvent] = []

		# Grab from url
		self.logging.info("Pulling from veyepar API")
		self.logging.info("Get %s", self.url)
		async with httpx.AsyncClient() as client:
			r = await client.get(self.url)
		results = r.json()

		self.logging.info("loaded %s talks.", len(results))

		# Should have data here, bail if we don't
		if len(results) == 0:
			sys.exit(-1)

		events: Dict[int, List[Event]] = defaultdict(list)
		for event in results:
			EventStart = datetime.fromisoformat(event["start"])
			if EventStart.tzinfo == None: #If event has no timezone, apply default timezone from settings
				EventStart = self.timezone.localize(EventStart)
			startTime = int(EventStart.timestamp())
			h,m,s = list(map(int,event["duration"].split(":")))
			Duration = timedelta(hours=h, minutes=m, seconds=s)

			# Need (room name)  (time-of-talk) <speaker name> - <talk name>>
			# Build internal data structure
			if event["name"] and event["authors"]: # Make sure we only append talks with non empty title and author
				events[startTime].append(
					{
						"title": event["name"],
						"code": event["id"],
						"room": event["location"],
						"speakers": [speaker.strip() for speaker in event["authors"].split(",")],
						# "speakerCodes": [speaker["code"] for speaker in event["speakers"]],
						"pre_record": event["prerecord"],#True if  else False, # TODO - Implement this field when there is a spec
						"start": EventStart,
						"end": EventStart + Duration,
					}
				)
		self.events = dict(events)  # Erase previous data if it existed

	def eventsAtTime(self, time: int) -> List[Event]:
		events = []
		if time in self.events:
			events = self.events[time]
		return events

	def printEvents(self) -> None:
		for event in self.events:
			print("Time: {}".format(event))
			print(self.events[event])
