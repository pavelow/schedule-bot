# Discord schedule-bot

Discord bot for conference schedule reminders.

The bot was built to provide discord-backstage notifications of talks for speakers and the conference team during PycOnlineAU 2020 and LCA2021.
It could be used for other events that use discord for backstage comms with minimal effort.

The basic workflow:
- T-60: Talk notification in `speaker-checkin` channel.
  - Speaker is found, technical checks performed
- T-15: Talk notification in backstage room channel.
  - Timer link provided by bot for optional use by speaker & room team.
  - Speaker invited to join stream, final technical checks and preparation before going live.

Pull requests are welcome!

## Description
- Bot will pull json schedule from (veyepar | pretalx | json file | whatever) and provide discord alerts an hour before and 15 minutes before, tagging speakers (assuming discord names contain their schedule names).
- Mappings from room names in schedule to discord channel IDs are in `channel_map.json` (notifications go to talks room channel).
- API tokens are stored in `api_tokens.json` or passed to docker run.
- Extra settings are stored in `settings.json`, if no timezone is given in the schedule, `timezone` from `settings.json` is used.
<!-- -  `pretalx_endpoints.json` contains the api endpoints to use for getting the schedule and the speaker data. -->

## Cloning
There is a submodule for generating talk timer urls, so need to recursively clone: `git clone --recurse https://gitlab.com/pavelow/schedule-bot.git`

## Usage
- On the host, populate `api_tokens.json` and `channel_map.json`
  Channel mappings are mappings from room name string to discord room channel id.

- `pip3 install -r requirements.txt` to install dependencies.
- `./schedule-bot.py -a file -s schedule.json` to run using `schedule.json` as the schedule with the `file` module.
- For API module details, see `sch_api/README.md`
- For testing, `-t 15.1` arguments will add add a 15 minute and 6 second offset to the start of the first event in `schedule.json` so that <n> minute messages can be verified. `-c` will print the config, and `-p` will print the schedule data.

```
usage: schedule_bot.py [-h] [-c] [-n] [-p] [-t TIME] [-a API] [-s SOURCE] [--token TOKEN]
                       [--discord_token DISCORD_TOKEN]

Discord schedule bot

optional arguments:
  -h, --help            show this help message and exit
  -c, --config          Show configuration
  -n, --noconnect       Don't connect to discord
  -p, --print           Print events - for testing
  -t TIME, --time TIME  Set start time for first event with offset in minutes - for testing
  -a API, --api API     Set API interface to use, must be name of a module in sch_api
  -s SOURCE, --source SOURCE
                        Filename or API endpoint location
  --token TOKEN         pass in API token
  --discord_token DISCORD_TOKEN
                        pass in discord token
```

## Bot Commands
Bot commands can be issued in the `bot-status` channel on discord.
- `!r` - refresh the user list and display unmatched names.

## Deploying
Deployment is implemented using gitlab CI (`gitlab-ci.yml`).
- Setup a Linux box with docker & SSH.
- Setup SSH keys.
- Clone this repo into `~/`.
- Run `docker build -t schedulebot:latest .` && `docker run --name schedulebotCI -d schedulebot`.
  - Creates container called `schedulebotCI`, CI script assumes this container exists.
- Populate CI/CD variables `Backend`, `BackendEndPoint`, `discordToken`, `PROD_SERVER_IP`, `SSH_PRIVATE_KEY`.
- Uncomment the `deploy` job in `gitlab-ci.yml`.
