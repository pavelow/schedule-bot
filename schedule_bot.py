#! /usr/bin/env python

# Discord schedule bot

import argparse
import asyncio
import copy
from collections import defaultdict
import json
import logging
import sys
from datetime import datetime, timedelta, timezone
import pytz
from os import path
from typing import Dict, List, Optional, TypedDict

import discord
import httpx
from discord.ext.tasks import loop
from discord.user import User
from typing import Optional
import importlib
import re

from countdownwidget.timerURLGen import TimerURLGenerator

logging.basicConfig(
	level=logging.INFO,
	force=True,
	format="%(asctime)-15s|%(name)-20s|%(levelname)-7s: %(message)s",
)


class Room(TypedDict):
	en: str


class Slot(TypedDict):
	start: str
	end: str
	room: Room


class Speaker(TypedDict):
	name: str
	code: str


class ApiEvent(TypedDict):
	title: str
	code: str
	slot: Slot
	speakers: List[Speaker]
	internal_notes: str


class Event(TypedDict):
	title: str
	code: str
	speakers: List[str]
	speakerCodes: List[str]
	pre_record: bool
	start: datetime
	room: str
	end: datetime


class bot(discord.Client):
	schedule: "scheduleWrangler"

	def __init__(self, config, schedule, debug="", idmap={}, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.config = config
		self.schedule = schedule
		self.channels = {}
		self.urlGen = TimerURLGenerator(config.settings["timerBaseURL"])
		self.urlGen.addReminder(timedelta(minutes=5), 1) #Chime once at 5 minute left
		self.urlGen.addReminder(timedelta(minutes=2), 1) #Chime once at 2 minute left
		self.urlGen.addReminder(timedelta(minutes=1), 1) #Chime once at 1 minute left
		self.id_map = idmap # Speaker Discord ID Map
		self.cmd = re.compile(r"^![a-z,A-Z]+") # Match command syntax
		self.commands = {
					"!r":self.cmd_refresh_speakers
				}
		self.missing = set()

	async def on_ready(self):
		logging.info("Logged on as %s", self.user)
		#await client.change_presence(activity=discord.Game(name='the game'))

		await self.schedule.import_talks()

		self.channels = {
			channel: self.get_channel(int(self.config.channel_map[channel]))
			for channel in self.config.channel_map
		}

		print(self.channels)
		self.check_time.start()
		# self.check_missing_speakers.start()
		self.update_schedule.start()
		await self.channels["bot-status"].send("Bot Initialised")

	# @loop(seconds=60 * 10)
	async def check_missing_speakers(self, p=False):
		missing_speakers = {
			speaker
			for events in self.schedule.events.values()
			for event in events
			for speaker in event["speakers"]
			if not self.lookup_speaker(speaker)
		}
		if missing_speakers != self.missing:
			self.missing = missing_speakers
			logging.info("Missing speakers: %s", missing_speakers)
			if len(missing_speakers) > 0 and p:
				await self.channels["bot-status"].send(
					"Could not match the following names: " + str(missing_speakers)
				)

		return missing_speakers  # for testing

	async def cmd_refresh_speakers(self, message):
		missing = await self.check_missing_speakers()
		await self.channels["bot-status"].send(
			"Refreshed list, could not match the following names: " + str(missing)
		)

	async def on_message(self, message):
		if message.author == self.user or message.channel.id != self.channels["bot-status"].id:  # don't respond to ourselves or other channels
			return

		re_matches = self.cmd.match(message.content)

		if (re_matches):
			logging.info("Got command: %s from %s", message.content, message.author)
			if(re_matches.string in self.commands):
				await self.commands[re_matches.string](message)

	def tagSpeakers(self, speakers):
		tags = []
		s = copy.deepcopy(speakers)
		logging.info("Tagging for %s", s)

		for speaker in s:
			user_id = self.lookup_speaker(speaker)
			logging.info("Lookup of %s got %s", speaker, user_id)
			if user_id:  # Tag speakers if nickname found
				tags.append("<@{0}>".format(user_id))
				s.remove(speaker)

		tags.extend(s) #Add speakers we didn't find

		return ', '.join(tags)

	def getBackStageURL(self, talkCode):
		return "https://2020.pycon.org.au/program/{0}/?backstage=true".format(talkCode)

	def messageTitleString(self, event):
		string = "**{0} - {1}** ---- *{3}*\n{2}".format(
		event["room"], event["start"].astimezone(self.config.timezone), self.tagSpeakers(event["speakers"]), event["title"])
		logging.info("Event: %s, Speakers: %s", event["title"], event["speakers"])
		# string = "**{0} - {1}** ---- *{3}* - ({4})\n{2}".format(
		# event["room"], event["start"].astimezone(self.config.timezone), self.tagSpeakers(event["speakers"]), event["title"], self.getBackStageURL(event["code"]))
		return string

	## TODO: Implement this
	async def emailSpeakers(self, speakerCodes, event):
		pass

	def lookup_speaker(self, speaker: str) -> int:
		# Lookup in ID Map first
		for user in self.id_map:
			if str(speaker).lower() in user.lower():
				return self.id_map[user]

		# if speaker wasn't found, try to string match
		for user in self.get_all_members(): #Get first instance of user with speaker_name
			if str(speaker).lower().replace(" ", "") in str(user.display_name).lower().replace(" ", ""):
				return user.id

	@loop(seconds=60 * 5) # Update talks every 5 minutes, in case there were any changes
	async def update_schedule(self):
		await self.schedule.import_talks()
		await self.check_missing_speakers(p=True)

	# Gets current unix time, search dict for time, trigger messaging if found
	# Is this a terrible way to do it?
	@loop(seconds=1)
	async def check_time(self) -> None:
		current_time = datetime.now(tz=self.config.timezone)
		# logging.info('Checking for new alerts at %s', current_time.isoformat())

		index = int(current_time.timestamp())
		delta24h = timedelta(hours=24).seconds
		delta90m = timedelta(minutes=90).seconds
		delta60m = timedelta(minutes=60).seconds
		delta15m = timedelta(minutes=15).seconds

		events60m = self.schedule.eventsAtTime(index + delta60m)
		events15m = self.schedule.eventsAtTime(index + delta15m)

		for event in events60m:
			# Send different t-60 message for pre-record
			if event["pre_record"]:
				string = "{0}\n(Pre-Recorded talk) If you would like to do live Q&A, we are ready for your Tech Check. Please confirm you are here!".format(
					self.messageTitleString(event)
				)
			else:
				string = "{0}\nWe are almost ready for your Tech Check, please confirm you are here!".format(
					self.messageTitleString(event)
				)
			logging.info("Sending ---%s--- to speaker-checkin", string)
			await self.channels["speaker-checkin"].send(
				string
			)  # speaker-checkin

		for event in events15m:
			# Should check if talk is pre-recorded here, once data is available
			if event["pre_record"]:
				string = "{0}\nHas pre-recorded talk".format(
					self.messageTitleString(event)
				)
				logging.info('Prerecorded end %s', self.urlGen.getAbsoluteTimeURL(event["end"]))
			else:
				string = "{0}\nWe are almost ready to invite you to your stream. Please confirm you are here!\nTalk Timer: {1}".format(
					self.messageTitleString(event),
					self.urlGen.getAbsoluteTimeURL(event["end"]),
				)

			logging.info("Sending ---%s--- to %s", string, event["room"])
			await self.channels[event["room"]].send(string)  # in relevant green room


class configuration:
	def __init__(self, schedule="schedule.json"):
		self.tokens = {}
		self.channel_map = {}
		if path.isfile("api_tokens.json"):
			with open("api_tokens.json") as f:
				self.tokens = json.load(f)
		else:
			self.tokens = {"APItoken": "", "discordToken": ""}

		with open("channel_map.json") as f:
			self.channel_map = json.load(f)
		# with open("pretalx_endpoints.json") as f:
		# 	self.endpoints = json.load(f)
		with open("settings.json") as f:
			self.settings = json.load(f)
		self.schedule = schedule
		# self.pretalx_token = {
		# 	"Authorization": "Token {0}".format(self.tokens["pretalxToken"])
		# }

		self.timezone = pytz.timezone(self.settings["timezone"])

	def show(self):
		print("Tokens:\n{0} \nChannel Map:\n{1}\nSchedule file: {2}".format(self.tokens,self.channel_map,self.schedule))

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="Discord schedule bot")
	parser.add_argument('-c', '--config', action="store_true", help="Show configuration")
	parser.add_argument('-n', '--noconnect', action="store_true", help="Don't connect to discord")
	parser.add_argument('-p', '--print', action="store_true", help="Print events - for testing")
	parser.add_argument('-i', '--idmap', action="store", help="ID Mapping file for speakers that aren't matched by name")
	parser.add_argument('-t', '--time', action="store", help="Set start time for first event with offset in minutes - for testing")
	parser.add_argument('-a', '--api', action="store", help="Set API interface to use, must be name of a module in sch_api")
	parser.add_argument('-s', '--source', action="store", help="Filename or API endpoint location")
	parser.add_argument('--token', action="store", help="pass in API token")
	parser.add_argument('--discord_token', action="store", help="pass in discord token")
	args = parser.parse_args()

	conf = configuration() # if not args.file else configuration(schedule=args.file)

	if args.token:
		conf.tokens["APItoken"] = args.token

	if args.discord_token:
		conf.tokens["discordToken"] = args.discord_token


	if args.api:
		mod = importlib.import_module("sch_api."+args.api)
		scheduleWrangler = getattr(mod, "scheduleWrangler")
		if(args.source): # if we passed in the schedule source
			tkn = args.token if args.token else "" #Pass API token in if exists
			schedule = scheduleWrangler(logging, conf.timezone, token=tkn,  debug=args.time, url=args.source)
		else:
			schedule = scheduleWrangler(logging, conf.timezone, debug=args.time)
	else:
		print("No API selected")
		sys.exit()

	if args.config:
		conf.show()

	# Load IDMap file
	idmap = {}
	if args.idmap:
		with open(args.idmap) as f:
			idmap = json.loads(f.read())

	if not args.noconnect:
		client = bot(conf, schedule, debug=args.time, idmap=idmap)
		client.run(conf.tokens["discordToken"])
	elif args.print: # If we want to just get and print events:
		loop = asyncio.get_event_loop()
		loop.run_until_complete(schedule.import_talks())
		schedule.printEvents()
