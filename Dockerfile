FROM python
COPY . /schedule-bot
WORKDIR /schedule-bot
ENV DISCORD_TOKEN ""
ENV BACKEND ""
ENV API_ENDPOINT ""
RUN pip3 install -r requirements.txt
CMD python3 schedule_bot.py --discord_token $DISCORD_TOKEN -a $BACKEND -s $API_ENDPOINT -i id_map.json
