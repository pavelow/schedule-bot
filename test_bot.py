from datetime import datetime, timedelta, timezone
import pytz
from unittest.mock import AsyncMock, MagicMock

from discord.channel import TextChannel
from discord.guild import Guild
from discord.http import HTTPClient
from discord.state import ConnectionState
from pytest import fixture, mark
import logging

from schedule_bot import bot as _bot
from schedule_bot import configuration
from sch_api.file import scheduleWrangler # only test file API :')

logging.basicConfig(
	level=logging.INFO,
	force=True,
	format="%(asctime)-15s|%(name)-20s|%(levelname)-7s: %(message)s",
)


@fixture
def http():
    return MagicMock(
        spec=HTTPClient,
        send_message=AsyncMock(
            return_value={
                "id": 1,
                "attachments": [],
                "embeds": [],
                "edited_timestamp": "2020-01-01T01:01",
                "type": "text",
                "pinned": False,
                "mention_everyone": False,
                "tts": False,
                "content": "",
            }
        ),
    )


@fixture
def state(http):
    cs = ConnectionState(
        dispatch=MagicMock(),
        handlers=MagicMock(),
        hooks=MagicMock(),
        syncer=MagicMock(),
        http=http,
        loop=MagicMock(),
    )
    guild = Guild(
        data={
            "id": 1,
            "members": [
                {
                    "user": {
                        "id": 4,
                        "username": "Frankfurt Speakerson",
                        "discriminator": "0000",
                        "avatar": "",
                    },
                    "roles": [],
                }
            ],
        },
        state=cs,
    )
    cs._guilds = {"Server": guild}
    return cs


@fixture
@mark.asyncio
async def bot(state):
    config = configuration()
    b = _bot(config=config, schedule=scheduleWrangler(logging, pytz.timezone("Australia/Melbourne")))
    b._connection = state
    b.channels = {
        "Basement": TextChannel(
            state=state,
            data={"id": 1, "type": "text", "name": "Basement", "position": 1},
            guild=b._connection._guilds["Server"],
        )
    }
    await b.schedule.import_talks()
    return b


@mark.asyncio
async def test_z(bot, monkeypatch, http):
    tz = timezone(timedelta(hours=8))

    TIME = datetime(2020, 1, 1, 1, 1, tzinfo=tz)
    monkeypatch.setattr("schedule_bot.datetime", MagicMock(now=lambda tz: TIME))

    bot.schedule.events[datetime(2020, 1, 1, 1, 1 + 15, tzinfo=tz).timestamp()] = [
        {
            "pre_record": True,
            "room": "Basement",
            "code": "test",
            "start": datetime(2020, 1, 1, 2, 0, tzinfo=tz),
            "end": datetime(2020, 1, 1, 2, 30, tzinfo=tz),
            "speakers": ["Tom", "Dick", "Harry"],
            "title": "Hello World.py",
        },
        {
            "pre_record": False,
            "room": "Basement",
            "code": "test",
            "start": datetime(2020, 1, 1, 2, 0, tzinfo=tz),
            "end": datetime(2020, 1, 1, 2, 30, tzinfo=tz),
            "speakers": ["Tom", "Dick", "Harry"],
            "title": "Hello World.py",
        },
    ]

    bot.check_time

    await bot.check_time.coro(bot)

    calls = http.send_message.mock_calls
    assert (
        calls[0].args[1]
        == "**Basement - 2020-01-01 05:00:00+11:00** ---- *Hello World.py*\nTom, Dick, Harry\n"
        + "Has pre-recorded talk"
    )
    assert (
        calls[1].args[1]
        == "**Basement - 2020-01-01 05:00:00+11:00** ---- *Hello World.py*\nTom, Dick, Harry\n"
        + "We are almost ready to invite you to your stream. Please confirm you are here!\n"
        + "Talk Timer: https://nextdayvideo.com/tools/countdown-widget/?t=2020-01-01T02:30:00+08:00&r=300-1/120-1/60-1/"
    )


# @mark.asyncio
# async def test_missing_speakers(bot):
#     bot.check_missing_speakers
#
#     missing = await bot.check_missing_speakers()
#
#     assert missing == {'Speaker Speakerson'}
